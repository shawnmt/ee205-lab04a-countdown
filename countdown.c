///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// Reference time:  Tue Jan 21 04:26:07 PM HST 2014
//
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 57 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 58 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 59 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 0 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 1 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 2 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 3 
//
// @author Shawn Tamashiro <shawnmt@hawaii.edu>
// @date   11 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

//macro time conversions to seconds

#define S_YEAR 31536000
#define S_DAY 86400
#define S_HOUR 3600
#define S_MIN 60

int main(int argc, char* argv[]) {
   //define datatype to points in time
   time_t ref, current;
   //declare intergers
   int year, day, hour, min, sec, diff;

   //reference time
   struct tm r_date = {
      .tm_sec = 7,            //seconds (0-59)
      .tm_min = 26,           //minutes (0-59)
      .tm_hour = 4,           //hours (0-23)
      .tm_mday = 21,          //day of month (1-31)
      .tm_mon = 0,            //month (0-11)
      .tm_year = 2014 - 1900  //# of year since 1900
   };

   //make reference time tim_t
   ref = mktime(&r_date);
   //print the reference 
   printf("Reference time: %s\n", asctime(&r_date));

   while(1){
      //get the current time
      current = time(NULL);

      //determine the difference between the reference time and current time
      //make absolute so we can go forward and backwards
      diff = abs(difftime(ref, current));

      //Calculate from difference in seconds using macro time conversions
      year = diff / S_YEAR;
      diff -= (year * S_YEAR);
      day = diff / S_DAY;
      diff -= (day * S_DAY);
      hour = diff / S_HOUR;
      diff -= (hour * S_HOUR);
      min = diff / S_MIN;
      sec = diff - (min * S_MIN);

      //print updated difference
      printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", year, day, hour, min, sec);

      sleep(1); //wait 1 second
   }

   return 0;
}

